function n = gmf_number_pairwise(GM)
% Number of pairwise potentials in a GMF

N = cellfun(@numel,GM.filter.NI);
n = sum(prod(N(GM.PI),1));
