function E = gmf_pw_fun_dummy(i, Ni, j, Nj, GM, EG)
% A dummy function to compute pairwise terms by simply looking up in another GM
% 

loc = EG(i,j);
assert(loc>0);
order = loc>0;
loc = abs(loc);

% Locate corresponding energies
if size(GM.PI,1)==3,
    k = GM.PI(3,loc);
else
    k = loc;
end

% Take relevant submatrix
if order,
  E = GM.PE{k}(Ni,Nj);
else
  E = GM.PE{k}(Nj,Ni)';
end

