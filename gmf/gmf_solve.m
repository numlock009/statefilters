function [energy,solution,info] = gmf_solve(GM,varargin)

read_varargin;
default verbose true;

% use the filter to generate the correct UE,PE,PI for use in gm_solve
UE = GM.UE;
PE = GM.PE;
PI = GM.PI;
for i=1:length(UE),
    UE{i} = UE{i}(GM.filter.NI{i});
end
np = 0;
nt = 0;
for k=1:size(PI,2),
    i = PI(1,k);
    j = PI(2,k);
    if size(PI,1)==3,
        K = PI(3,k);
    else
        K = k;
    end
    PE{K} = PE{K}(GM.filter.NI{i},GM.filter.NI{j});
    %np = np + numel(GM.filter.NI{i})*numel(GM.filter.NI{j});
    %nt = nt + numel(GM.PE{K});
end
filteredGM.PI = PI;
filteredGM.PE = PE;
filteredGM.UE = UE;

if verbose, fprintf('Solving for pruned GM\n'); end
[energy,solutionF,info] = gm_solve(filteredGM, varargin{:}); % solution in filtered problem
if verbose, fprintf('Casting solution back to original GM\n'); end
solution = cellfun(@(x,y) x(y),GM.filter.NI,num2cell(solutionF)); % solution in original problem
info.filtered_solution = solutionF;
info.npairwise = gm_number_pairwise(filteredGM);

