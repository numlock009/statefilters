function [GM,info] = gmf_apply_filter(filter,GM,G,EG,pw_fun,pw_fun_args)

if filter.unary,
    [features,GM,info] = gmf_extract_unary_features(GM,G,EG,pw_fun,pw_fun_args);
else
    [features,GM,info] = gmf_extract_maxmarginal_features_from_randspantree(GM,G,EG,pw_fun,pw_fun_args);
end

minStates = filter.limit;
for i=1:length(features),
    [scores,idx] = filter_logdis(filter,features{i});
    if nnz(idx)<minStates, % ensure at least minStates states for each node
        [~,idxF] = top(scores,minStates);
        idx(idxF) = 1;
    end
    GM.filter.NI{i} = GM.filter.NI{i}(idx);
end
