function [features,GM,info] = gmf_extract_unary_features(GM,G,EG,pw_fun,pw_fun_args)

info.newpairwise = 0;
% get unaries of filtered GM
M = GM.UE;
for i=1:length(M),
    M{i} = M{i}(GM.filter.NI{i});
end
% transform unaries into a feature vector
features = features_from_unaries(M);
info.new_pairwise = 0;

