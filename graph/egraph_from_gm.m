function G = egraph_from_gm(GM)

N = length(GM.UE);
I = double(GM.PI);
nEdges = size(I,2);

if size(I,1)==2,
    k = 1:nEdges;
else
    k = double(I(3,:));
end

G = full(sparse([I(1,:) I(2,:)],[I(2,:) I(1,:)],[k -k],N,N));
 
end
