function G = graph_from_gm(GM,varargin)

read_varargin;
default value 1;

N = length(GM.UE);
I = double(sort(GM.PI,1,'descend'));
G = sparse(I(1,:),I(2,:),value,N,N);

end
