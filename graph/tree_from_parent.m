function tree = tree_from_parent(parent)
% Make a tree from a parent vector

n = length(parent);
tree.parent = parent;
tree.root   = find(parent==0);
tree.leaves = setdiff(1:n,parent);
% order tree nodes for easy traversal
tree = tree_toposort(tree);

end
