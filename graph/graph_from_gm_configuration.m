function [G,E] = graph_from_gm_configuration(GM,state)

N = length(GM.UE);
G = zeros(N,N);
E = 0;
for k=1:length(GM.UE), % add unaries
    E = E+GM.UE{k}(state(k));
end
for k=1:length(GM.PE), % build the energy graph of the configuration
    i = GM.PI(1,k);
    j = GM.PI(2,k);
    G(i,j) = GM.PE{k}(state(i),state(j));
    G(j,i) = G(i,j);
end
G = sparse(tril(G));
