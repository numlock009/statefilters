function GM = tgm_from_egraph_gm_tree(GM,EG,tree)
% Add given tree structure on top of GM, exploiting EG edge indices.

n = length(tree.parent);
tree.parent_edge_index = zeros(1,n);
tree.parent_edge_order = true(1,n);

notroot = tree.parent~=0;
loc = EG(sub2ind([n n],[1:tree.root-1 tree.root+1:n],tree.parent(notroot)'));

tree.parent_edge_order(notroot) = loc>0;
loc = abs(loc);

if size(GM.PI,2)==3,
    loc = GM.PI(3,loc);
end
tree.parent_edge_index(notroot) = loc;

GM.tree = tree;

%for i=1:length(tree.parent),
%    j=tree.parent(i);
%    if j>0,
%        loc = EG(i,j);
%        order = loc>0;
%        loc = abs(loc);
%        if size(GM.PI,2)==3,
%            assert(tree.parent_edge_index(i) == GM.PI(3,loc));
%        else
%            assert(tree.parent_edge_index(i) == loc);
%        end
%        assert(tree.parent_edge_order(i) == order);
%    end
%end

