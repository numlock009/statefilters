function GM = tgm_from_gm_randspantree(GM)

% Get the graph of the GM
G = graph_from_gm(GM);
% Sample a random spanning tree from the graph
tree = graph_randspantree(G);
% Add the tree structure to the GM
GM = tgm_from_gm_tree(GM,tree);

