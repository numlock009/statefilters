function [energy,solution] = tgm_solve_bp(GM)
% The input *must be* a tree. 
assert(isfield(GM,'tree'));

DB=0;
if DB, tt=tic(); end

n = numel(GM.UE);   % number of nodes
S = cell(1,n);      % state-memory
M = cell(1,n);      % messages
solution = zeros(1,n,'uint16');

% First pass from leaves to root: compute min-energy and trace
for u=1:n,
    i = GM.tree.leaves2root(u);
    M{i} = GM.UE{i}; % prepare message from i with unaries (that's all if leaf node)
    s    = find(GM.tree.parent==i); % collect children messages
    if DB && isempty(s),
        fprintf('Node %d: init leaf message\n',i);
    end
    for k=1:length(s),
        j = s(k);
        if DB, fprintf('Node %d: message from %d\n',i,j); end;
        e = GM.tree.parent_edge_index(j);
        f = GM.tree.parent_edge_order(j); % 1->energy from i to j; 0-> from j to i
        if f,
            [Mij,S{j}] = min(bsxfun(@plus,M{j}',GM.PE{e}));
        else
            [Mij,S{j}] = min(bsxfun(@plus,M{j}',GM.PE{e}'));
        end
        % PE = GM.PE{pe2(k)}; % pairwise energy for (i,j)
        % PE = bsxfun(@plus,M{j}',PE); % add the message at node j
        % [Mij,S{j}] = min(PE); % find minimum and keep track of best states of j for the states of i
        M{i} = M{i} + Mij;
        %M{j} = []; % free memory from useless messages
    end
end
[energy,solution(GM.tree.root)] = min(M{GM.tree.root});
if DB, fprintf('energy: %f (%fs)\n',energy,toc(tt)); end

% Back-trace: recover configuration
if nargout>1,
for u=2:n,
    i = GM.tree.root2leaves(u);
    pi = GM.tree.parent(i);
    solution(i) = S{i}(solution(pi));
end
if DB, fprintf('solution found: %fs\n',toc(tt)); end
end

end
