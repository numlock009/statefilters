%%% Demonstration code fro Learnt State Filters %%%
addpath('demo');

% Load pre-computed discrete pairwise energy minimization problem
fprintf('Loading image and pre-computed pairwise terms\n\n\n');
load('demo/img1.mat');

% Use gm_solve to compute solution and energy.
% Available methods: LoopyBP, TRW-S, Alpha-expansion, Alpha-expand beta-shrink
% Filtering methods available: Best unaries, Learnt State Filters.
% check:
% >> help gm_solve;

fprintf('======== Solve with TRW-S ========\n');
fprintf('Using precomputed pairwise (%d)\n',gm_number_pairwise(GM));
[energyTRWS,solutionTRWS,infoTRWS] = gm_solve(GM,...
    'method','trws','verbose',true,'maxIter',30);
fprintf('========= TRW-S finished =========\n');
fprintf('Energy: %f\n',energyTRWS);
fprintf('Number of used pairwise: %d\n',gm_number_pairwise(GM));
fprintf('Time to infer: %fs\n',infoTRWS.infertime);
fprintf('==================================\n\n\n');

fprintf('======= Solve with Filters =======\n');
fprintf('Loading filter model from disk\n');
load('demo/model_semanticsegmentation_t10l2.mat');
GM_pruned = GM;
GM_pruned.PE = cell(1,length(GM_pruned.PE));
[energyFilters,solutionFilters,infoFilters] = gm_solve(GM_pruned,...
    'method','statefilters','verbose',false,'submethod','trws','model',model,'pw_function',{@pw_dummy,GM});
fprintf('======== Filters finished ========\n');
fprintf('Energy: %f\n',energyFilters);
fprintf('Number of used pairwise: %d\n',infoFilters{end}.total_pw);
fprintf('Time to infer: %fs\n',infoFilters{end}.infertime);
fprintf('==================================\n\n\n');

fprintf('============ Summary =============\n');
fprintf('Solution agreement: %3.1f%%\n',100*nnz(solutionTRWS==solutionFilters)/length(solutionTRWS));
fprintf('PW speed-up: %2.1fx\n',gm_number_pairwise(GM)/infoFilters{end}.total_pw);
fprintf('Inference speed-up: %2.1fx\n',infoTRWS.infertime/infoFilters{end}.infertime);
fprintf('==================================\n\n\n');

