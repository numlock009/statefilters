
assert(~isempty(getenv('MATLABTOOLS')),'You need to install and load matlabtools first: https://bitbucket.org/mguillau/matlabtools');
run([getenv('MATLABTOOLS') filesep 'setup.m']);

sf_fileName = mfilename('fullpath');
sf_dirName = fileparts(sf_fileName);

sf_subdirs = { '3rdparty', 'expe', 'filter', 'gm', 'gmf', 'graph', 'tgm', 'tgmf' };

for sf_s=1:length(sf_subdirs),
  addpath(genpath([sf_dirName filesep sf_subdirs{sf_s}]));
end

clear sf_*
