function energy = gm_energy_of_configuration(GM,state)
% GM_ENERGY_OF_CONFIGURATION Read out the energy of a GM corresponding to a configuration (states)
%
% energy = gm_energy_of_configuration(GM,states)
%

energy = 0;

% Add unary energies
for i=1:length(state),
    energy = energy + GM.UE{i}(1,state(i));
end

% Detect GM format
if size(GM.PI,1)==2,
    V = 1:size(GM.PI,2);
else
    V = GM.PI(3,:);
end

% Add pairwise energies
for v=1:length(V),
    i = GM.PI(1,v);
    j = GM.PI(2,v);
    energy = energy + GM.PE{V(v)}(state(i),state(j));
end
