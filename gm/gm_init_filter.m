function GM = gm_init_filter(GM)

GM = rmfield2(GM,'filter');
GM.filter.NI = cell(size(GM.UE)); % kept states
for i=1:length(GM.UE),
    GM.filter.NI{i} = 1:length(GM.UE{i});
end

