function n = gm_number_pairwise(GM)
% Number of pairwise potentials in a GM

n = sum(prod(cellsizes(GM.PE),2));
