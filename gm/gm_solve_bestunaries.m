function [energy,solution,info] = gm_solve_bestunaries(GM,k,varargin)
% Solve GM faster by restricting states to the best k.
%
% If k is a float, consider it as a fraction of states to keep.

read_varargin;
defaultstr submethod 'trws';
default verbose true;

% Initialise filter and keep only the best k states based on unaries
if round(k)==k,
   if verbose, fprintf('Keeping only %d states\n',k); end
else
   if verbose, fprintf('Keeping only %.1f%% of the states\n',100*k); end
end

GMf = gm_filter_bestunaries(GM,k);

if verbose, fprintf('Pairwise potentials: %d -> %d\n',gm_number_pairwise(GMf),gmf_number_pairwise(GMf)); end

[energy,solution,info] = gmf_solve(GMf,varargin{:},'method',submethod);

kept_states = cellsizes(GMf.filter.NI);
info.number_kept_states = kept_states(:,1)';

