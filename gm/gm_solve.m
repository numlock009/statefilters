function [energy,solution,info] = gm_solve(GM,varargin)
% GM_SOLVE Find a low-energy configuration for a GM
%
% input: GM
%
% options:
%  - method [default=trws] --
%    'lbp' (Loopy belief propagation)
%    'trws (Sequential tree-reweighted message passing)
%    'alpha' (Alpha-expansion)
%    'expandshrink' (Alpha-expansion/beta-shrink)
%    'bestunaries' (Solve the problem restricted to the best k unaries)
%    'statefilters' (Prune the GM based on state filters)
%  - verbose (all) [default=true] -- true/false
%  - maxIter (lbp,trws) [default=30] -- maximum number of iterations
%  - betaSelect (expandshrink) [default=3] -- 0=alpha-expansion, 1=randomized selection of beta, 2=iterate over beta, 3=iterate over alpha, 4=set beta to alpha-1, 5=set beta to alpha+1
%  - solution (alpha,expandshrink) [default=unary argmin] -- initial solution
%  - submethod (bestunaries,filters) [default=trws] -- use submethod to solve the pruned GM
%  - k (bestunaries) [default=0.1] -- use k states, or k of the states if k is fractional
%  - model (filters) [required option] -- state filter model to use
%
% outputs:
%
%  energy : energy of the solution
%  solution : the solution
%  info : a struct with relevant information (inference time, lower bounds, number of moves, number of states kept, ... )

read_varargin;
default verbose true;
defaultstr method 'trws';

if strcmp(method,'statefilters'),
    assert_empty_gm(GM);
else
    assert_gm(GM);
end

switch method, % aliases
  case 'alpha',
    method = 'expandshrink';
    betaSelect = 0;
  case 'lbp',
    method = 'trws';
    useTRW = false;
end

switch method,
  case 'trws',
    default maxIter 30;
    default useTRW true;
    infertime = tic;
    if verbose,
      [solution,energy,info.energy_lb] = trw_bp_mex(GM.UE, GM.PI, GM.PE, int32([useTRW 0 maxIter]));
    else
      evalc('[solution,energy,info.energy_lb] = trw_bp_mex(GM.UE, GM.PI, GM.PE, int32([useTRW 0 maxIter]))');
    end
    info.infertime = toc(infertime);
  case 'expandshrink',
    default betaSelect 3;
    infertime = tic;
    [energy,solution,info] = gm_solve_expandshrink(GM,betaSelect,varargin{:});
    info.infertime = toc(infertime);
  case 'bestunaries',
    default k 0.1;
    [energy,solution,info] = gm_solve_bestunaries(GM,k,varargin{:});
  case 'statefilters',
    defaultstr submethod 'trws';
    [energy,solution,info] = gm_solve_statefilters(GM,model.filter,pw_function{1},pw_function(2:end));
  otherwise
    error('unknown or unimplemented method %s to solve gm',method);
end


