function [loc,order] = gm_find_edge(GM,i,j)

pi1 = GM.PI(1,:)==i;
pi1(pi1) = GM.PI(2,pi1)==j;
order = any(pi1);
if order, % the column is [i;j]
    loc = find(pi1);
else % the column is [j;i]
    pi2 = GM.PI(2,:)==i;
    pi2(pi2) = GM.PI(1,pi2)==j;
    loc = find(pi2);
end 

assert(length(loc)==1);
