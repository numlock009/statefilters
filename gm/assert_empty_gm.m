function assert_gm(GM)
% ASSERT_GM Check the validity of a GM struct
%
% Raises an assertion exception if the GM is invalid

u32max = 4294967295; %intmax('uint32');
u16max = 65535; %intmax('uint16');

% Check fields of GM struct
assert(isfield(GM,'UE'),'GM does not have unary energies (GM.UE)');
assert(isfield(GM,'PI'),'GM does not have edge indices (GM.PI)');
assert(isfield(GM,'PE'),'GM does not have edge energies (GM.PE)');

% Check unaries
assert(iscell(GM.UE),'Unary energies (GM.UE) not provided as cell array');
assert(size(GM.UE,1)==1,'Unary energies (GM.UE) should be a row cell array');
nNodes = size(GM.UE,2);
assert(nNodes<u32max,sprintf('No support for more than %s nodes',u32max));
U = cellsizes(GM.UE);
assert(all(U(:,1)==1),'All unaries should be row vectors');
assert(all(U(:,2)<u16max),sprintf('No support for more than %s labels',u16max));
assert(all(cellfun(@isdouble,GM.UE)),'All unaries should be double vectors');

% Check pairwise indices
assert(isa(GM.PI,'uint32'),'Invalid edge indices: GM.PI should be of type uint32');
assert(size(GM.PI,1) == 2 || size(GM.PI,1) == 3,'Invalid edge indices: GM.PI should have 2 or 3 rows');
nEdges = size(GM.PI,2);
vEdges = vec(GM.PI(1:2,:));
assert(all(vEdges>0),'1-based indexing!');
assert(all(vEdges<=nNodes),'Cannot connect inexistant nodes');

% Check pairwise energies
assert(iscell(GM.PE),'Pairwise energies (GM.PE) not provided as cell array');
assert(size(GM.PE,1)==1,'Pairwise energies (GM.PE) should be a row cell array');
if size(GM.PI,1)==2,
  assert( size(GM.PE,2)==size(GM.PI,2), 'GM.PE and GM.PI do not match' );
  I = uint32(1:nEdges);
else
  assert( size(GM.PE,2)>=max(GM.PI(3,:)), 'GM.PE too short' );
  I = GM.PI(3,:);
end

