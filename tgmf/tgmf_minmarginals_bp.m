function M = tgmf_minmarginals_bp(GM)
% Compute min-marginals of a spanning tree with filtered states of a GM.

DB=0;
tt=tic();

n   = numel(GM.UE); % number of nodes
M   = cell(n,n);    % messages (both forward and backward)
for u=1:n, % forward pass
    i = GM.tree.leaves2root(u);
    s = find(GM.tree.parent==i); % children nodes
    idxi = GM.filter.NI{i};
    ni = numel(idxi);
    M{i,i} = GM.UE{i}(idxi); % prepare message from i
    for k=1:length(s),
        j = s(k);
        if DB, fprintf('Node %d: message from %d\n',i,j); end
        e = GM.tree.parent_edge_index(j);
        f = GM.tree.parent_edge_order(j);
        idxj = GM.filter.NI{j};
        nj = numel(idxj);
        if f,
            M{j,i} = min(bsxfun(@plus,M{j,j}',GM.PE{e}(idxj,idxi)));
        else
            M{j,i} = min(bsxfun(@plus,M{j,j}',GM.PE{e}(idxi,idxj)'));
        end
        M{i,i} = M{i,i} + M{j,i};
    end
end

tt=tic();
for u=2:n, % backward pass
    i = GM.tree.root2leaves(u);
    j = GM.tree.parent(i);
    if j>0, % backward messages
        idxi = GM.filter.NI{i};
        idxj = GM.filter.NI{j};
        if DB, fprintf('Node %d: message from %d\n',i,j); end
        % M{j,j} contains the min-marginals. We need to substract the
        % message sent to it: M{i,j}, and add the forward message M{i,i}
        % M{i,i} becomes the min-marginal
        e = GM.tree.parent_edge_index(i);
        f = GM.tree.parent_edge_order(i);
        if f,
            M{j,i} = min(bsxfun(@plus,(M{j,j}-M{i,j})',GM.PE{e}(idxi,idxj)'));
        else
            M{j,i} = min(bsxfun(@plus,(M{j,j}-M{i,j})',GM.PE{e}(idxj,idxi)));
        end
        M{i,i} = M{i,i} + M{j,i};
    end
end

M = M(1:n+1:end); % min-marginals are on the diagonal
end
