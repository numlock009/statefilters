function [M,Mu,Mp] = tgmf_minmarginalssplit_bp(GM)
% Compute min-marginals of a spanning tree with filtered states of a GM.
% Splits the minmarginal M into unary Mu and pairwise Mp min-marginal contributions

assert(false,'to be fully checked');

DB=0;
tt=tic();

n   = numel(GM.UE); % number of nodes
M   = cell(n,n);    % messages (both forward and backward)
Mu  = cell(n,n);    % messages (both forward and backward)
Mp  = cell(n,n);    % messages (both forward and backward)

for u=1:n, % forward pass
    i = GM.tree.leaves2root(u);
    s = find(GM.tree.parent==i); % children nodes
    idxi = GM.filter.NI{i};
    ni = numel(idxi);
    M{i,i} = GM.UE{i}(idxi); % prepare message from i
    Mu{i,i} = GM.UE{i}(idxi);        % unary message
    Mp{i,i} = zeros(1,numel(idxi));  % pairwise message
    for k=1:length(s),
        j = s(k);
        if DB, fprintf('Node %d: message from %d\n',i,j); end
        e = GM.tree.parent_edge_index(j);
        f = GM.tree.parent_edge_order(j);
        idxj = GM.filter.NI{j};
        nj = numel(idxj);
        tMjj  = M{j,j}';
        tMujj = Mu{j,j}';
        tMpjj = Mp{j,j}';
        if f,
            % keep ij, the index of the minimum in message passing
            [M{j,i},ij] = min(bsxfun(@plus,tMjj,GM.PE{e}(idxj,idxi)));
            Mu{j,i} = tMujj(ij)'; % just read out corresponding unaries
            idx = (ij) + ((0:ni-1)*nj);
            mpji    = bsxfun(@plus,tMpjj,GM.PE{e}(idxj,idxi));
            Mp{j,i} = mpji(idx); % use ij to get the part of the message due to pairwise stuff
        else
            [M{j,i},ij] = min(bsxfun(@plus,tMjj,GM.PE{e}(idxi,idxj)'));
            Mu{j,i} = tMujj(ij)'; % just read out corresponding unaries
            idx = (ij) + ((0:ni-1)*nj);
            mpji    = bsxfun(@plus,tMpjj,GM.PE{e}(idxi,idxj)');
            Mp{j,i} = mpji(idx);
        end
        M{i,i} = M{i,i} + M{j,i};
        Mu{i,i} = Mu{i,i} + Mu{j,i};
        Mp{i,i} = Mp{i,i} + Mp{j,i};
    end
end

tt=tic();
for u=2:n, % backward pass
    i = GM.tree.root2leaves(u);
    j = GM.tree.parent(i);
    if j>0, % backward messages
        idxi = GM.filter.NI{i};
        idxj = GM.filter.NI{j};
        ni = numel(idxi);
        nj = numel(idxj);
        if DB, fprintf('Node %d: message from %d\n',i,j); end
        % M{j,j} contains the min-marginals. We need to substract the
        % message sent to it: M{i,j}, and add the forward message M{i,i}
        % M{i,i} becomes the min-marginal
        e = GM.tree.parent_edge_index(i);
        f = GM.tree.parent_edge_order(i);
        tMjji = (M{j,j}-M{i,j})';
        tMujji = (Mu{j,j}-Mu{i,j})';
        tMpjji = (Mp{j,j}-Mp{i,j})';
        if f,
            [M{j,i},ij] = min(bsxfun(@plus,tMjji,GM.PE{e}(idxi,idxj)'));
            Mu{j,i} = tMujji(ij)'; % just read out corresponding unaries
            idx = (ij) + ((0:ni-1)*nj);
            mpji = bsxfun(@plus,tMpjji,GM.PE{e}(idxi,idxj)');
            Mp{j,i} = mpji(idx);
        else
            [M{j,i},ij] = min(bsxfun(@plus,tMjji,GM.PE{e}(idxj,idxi)));
            Mu{j,i} = tMujji(ij)';
            idx = (ij) + ((0:ni-1)*nj);
            mpji = bsxfun(@plus,tMpjji,GM.PE{e}(idxj,idxi));
            Mp{j,i} = mpji(idx);
        end
        M{i,i} = M{i,i} + M{j,i};
        Mp{i,i} = Mp{i,i} + Mp{j,i};
        Mu{i,i} = Mu{i,i} + Mu{j,i};
    end
end
%fprintf('<%fs|',toc(tt));

M = M(1:n+1:end); % min-marginals are on the diagonal
Mu = Mu(1:n+1:end);
Mp = Mp(1:n+1:end);

end
