function X = features_from_minmarginals(M)

X = cell(size(M));
for k=1:length(M);
    mm = M{k};
    n = length(mm);
    minmm = mm-min(mm); % gap to smallest value
    [mms,mmi] = sort(mm); % sorted mm
    mmrank = invperm(mmi); % rank of mm

    dmms = [0 diff(mms)]; % difference to preceeding value (sorted mm)
    dmms = dmms(mmrank);  % re-organized

%    fracmm = minmm./max(minmm); % fraction of mm
%    cumfrac = cumsum(sort(fracmm));
%    cumfrac = cumfrac(mmrank);
%
%    X{k} = [ minmm; fracmm; dmms; mmrank; cumfrac; ];
%    X{k} = [ X{k}; 1./(1+X{k}); ones(1,n) ];

    X{k} = [minmm;mm./min(mm);dmms;mmi;1./mmi;ones(1,n)];

end

