**statefilters**
================

A MATLAB implementation of Learned State Filters.



---

What is it? 
-----------

This package is a MATLAB implementation of Learned State Filters for fast discrete pairwise energy minimization as presented
in the [following paper](http://biwinas03.ee.ethz.ch/mguillau/publications/Guillaumin2013cvpr.pdf):

    @InProceedings{Guillaumin2013cvpr,
    title = {{Fast Energy Minimization using Learned State Filters}},
    author = {Guillaumin, Matthieu and Van Gool, Luc and Ferrari, Vittorio},
    booktitle = {{IEEE Conference on Computer Vision \& Pattern Recognition (CVPR)}},
    year = {2013},
    month = jun,
    pdf = {http://biwinas03.ee.ethz.ch/mguillau/publications/Guillaumin2013cvpr.pdf}
    }

Please cite this work if you use this package for publication.

For convenient comparison to other energy minimization work, we have also included code for *TRW-S*, *alpha-expansion*, *alpha-expand beta-shrink* and *loopy belief propagation*.

How to install? 
---------------

1. First, download and install Matlabtools: [bitbucket:matlabtools](https://bitbucket.org/mguillau/matlabtools).

2. To activate it in Matlab, run:

        run('/path/to/matlabtools/setup.m');

3. Optionally, download pre-computed data and models from [here](http://vision.ee.ethz.ch/~guillaumin/data/statefilters).


How to use? 
-----------

After you have followed the installation and configuration steps above, you
simply need to run `setup.m` in MATLAB:

    run('/path/to/statefilters/setup.m');

Then you can run the simple demo code:

    demo();

The main entry point for this toolbox is the `gm_solve` function. Usage of this function is described in its help section and `demo.m` is an example use of TRW-S and state filters on pre-computed data and models.

